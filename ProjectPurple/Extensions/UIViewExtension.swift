//
//  UIViewExtension.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/7/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

extension UIView {
    func shadow(oppacity: Float = 0.35) {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowOpacity = oppacity
    }

    func roundCorners(radius: CGFloat = 5) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }

    func topRoundedCorners(cornerRadius: CGFloat) {
        layer.cornerRadius = CGFloat(cornerRadius)
        clipsToBounds = true
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    func bottomRoundedCorners(cornerRadius: CGFloat = 5) {
        layer.cornerRadius = CGFloat(cornerRadius)
        clipsToBounds = true
        layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }

    func roundedCornersModalView() {
        layer.cornerRadius = 6
    }

    func addAndFill(view: UIView, padding: CGSize = CGSize(width: 0, height: 0)) {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-\(padding.width)-[child]-\(padding.width)-|", options: [], metrics: nil, views: ["child": view]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(padding.height)-[child]-\(padding.height)-|", options: [], metrics: nil, views: ["child": view]))
    }

    func circularView() {
        layer.cornerRadius = bounds.size.height / 2
        layer.masksToBounds = true
    }
}
