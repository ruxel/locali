//
//  UIViewControllerExtension.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/7/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

extension UIViewController {
    func showOkAlertWithMessage(title: String) {
        let alert = UIAlertController(title: nil, message: title, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: ls.ok(), style: .cancel))
        alert.modalPresentationStyle = .popover
        present(alert, animated: true, completion: nil)
    }

    func showGoToSettingsAlertWithMessage(_ message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alert.addAction(UIAlertAction(title: "Go to settings", style: .default, handler: { _ in
            Utils.showSettings()
        }))
        alert.modalPresentationStyle = .popover

        present(alert, animated: true, completion: nil)
    }

    func addTapGestureRecognizerToHideKeyboardTappingOutsideIt() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    func is_portrait() -> Bool {
        view.bounds.size.height > view.bounds.size.width
    }

    func is_landscape() -> Bool {
        !is_portrait()
    }
}
