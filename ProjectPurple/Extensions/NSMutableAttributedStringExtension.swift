//
//  NSMutableAttributedString.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/9/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {

    var fontSize: CGFloat {
        17
    }
    
    var boldFont: UIFont {
        UIFont.boldSystemFont(ofSize: fontSize)
    }

    var normalFont: UIFont {
        UIFont.systemFont(ofSize: fontSize)
    }

    func bold(_ value: String) -> NSMutableAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [.font: boldFont]
        self.append(NSAttributedString(string: value, attributes: attributes))
        return self
    }

    func normal(_ value: String) -> NSMutableAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [.font: normalFont]
        self.append(NSAttributedString(string: value, attributes: attributes))
        return self
    }
}
