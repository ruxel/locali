//
//  ButtonExtension.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/9/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

extension UIButton {
    func styleLogInBtn() {
        self.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }

    var text: String? {
        get {
            title(for: .normal)
        }

        set {
            if newValue != nil {
                setTitle(newValue!, for: .normal)
            }
        }
    }

    var textColor: UIColor? {
        get {
            titleColor(for: .normal)
        }

        set {
            if newValue != nil {
                setTitleColor(newValue, for: .normal)
            }
        }
    }

    func enableShrinkTitleToFit(withInsets: Bool = true) {
        titleLabel?.numberOfLines = 1
        titleLabel?.minimumScaleFactor = 0.2
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.lineBreakMode = .byClipping
        if withInsets { contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8) }
    }

    var textThatShrinks: String? {
        get {
            text
        }

        set {
            text = newValue
            enableShrinkTitleToFit()
        }
    }

    func tintedImage(image: UIImage, color: UIColor) {
        let img = image.withRenderingMode(.alwaysTemplate)
        self.setBackgroundImage(img, for: .normal)
        self.setBackgroundImage(img, for: .selected)
        self.tintColor = color
    }
}
