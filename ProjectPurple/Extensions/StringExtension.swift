//
//  StringExtension.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/7/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import Foundation

extension String {
    func toAppVersion() -> Int {
        var appVersionString = ""

        self.forEach { (char) in
            if char != "." { appVersionString.append(char) }
        }

        return Int(appVersionString) ?? 1000
    }
}
