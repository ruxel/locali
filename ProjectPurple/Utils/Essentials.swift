//
//  Essentials.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/5/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

///  Threading
public func delay(_ delay: Double, onBackground: Bool = false, closure: @escaping () -> ()) {
    let queue: DispatchQueue
    if onBackground {
        queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
    } else {
        queue = DispatchQueue.main
    }
    queue.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

public func onMain(_ closure: @escaping () -> ()) {
    DispatchQueue.main.async(execute: closure)
}

/// Basically just letting dispatch code on background or not without an if else
public func runSynchronously(_ synchronously: Bool = true, closure: @escaping () -> ()) {
    if (synchronously) {
        closure()
    } else {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async(execute: closure)
    }
}

public func onBackground(_ dispatchAgain: Bool = false, closure: @escaping () -> ()) {
    if Thread.isMainThread || dispatchAgain {
        MyLogI("dispatching onto new bg thread")
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async(execute: closure)
    } else {
        MyLogI("already on bg - just running code")
        closure()
    }
}


/// Logging
func MyLogI(_ msg: Any?) {
    if let message = msg as? CustomStringConvertible {
        NSLog("ℹ️ \(NSString(string: message.description))")
    } else {
        NSLog("ℹ️ \(NSString(string: msg.debugDescription))")
    }
}

func MyLogD(_ msg: Any?) {
    #if DEBUG
    if let message = msg as? CustomStringConvertible {
        NSLog("✳️ \(NSString(string: message.description))")
    } else {
        NSLog("✳️ \(NSString(string: msg.debugDescription))")
    }
    #endif
}

func MyLogE(_ msg: Any?) {
    if let message = msg as? CustomStringConvertible {
        NSLog("🛑 \(NSString(string: message.description))")
    } else {
        NSLog("🛑 \(NSString(string: msg.debugDescription))")
    }
}

let ls = R.string.localizable.self

typealias Callback = () -> ()
typealias SuccessCallback = (Bool) -> ()
typealias JSONObject = [String: Any]
typealias JSONArray = [[String: Any]]

///  Idiom
func is_pad() -> Bool {
    UIScreen.main.traitCollection.userInterfaceIdiom == .pad
}

func is_phone() -> Bool {
    !is_pad()
}
