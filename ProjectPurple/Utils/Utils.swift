//
//  Utils.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/7/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

final class Utils {

    // MARK: - UI methods
    static func topViewController() -> UIViewController {
        var topController = UIApplication.shared.keyWindow?.rootViewController
        while ((topController?.presentedViewController) != nil) {
            topController = topController?.presentedViewController
        }
        return topController!
    }

    static func startActivityIndicator(activityIndicator: UIActivityIndicatorView) {
        activityIndicator.startAnimating()
        activityIndicator.alpha = 1
    }

    static func stopActivityIndicator(activityIndicator: UIActivityIndicatorView) {
        activityIndicator.alpha = 0
        activityIndicator.stopAnimating()
    }

    static func showSettings() {
        if let settingsUrl = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: nil)
        }
    }

    static func performAddToCartAnimation() {
        guard let view = Utils.topViewController().view else { return }

        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        if #available(iOS 13.0, *) {
            button.setImage(UIImage(systemName: "cart"), for: UIControl.State())
            button.setPreferredSymbolConfiguration(.init(pointSize: 26), forImageIn: UIControl.State())
        } else {
            // Fallback on earlier versions
        }
        button.tintColor = R.color.localiGreen()

        view.addSubview(button)

        var topDistance: CGFloat = 80
        if let safeAreaPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top {
            topDistance += safeAreaPadding
        }

        let frameWidth = view.frame.width
        let startPoint = CGPoint(x: frameWidth - 65, y: topDistance)
        let controlPoint = CGPoint(x: frameWidth - 50, y: 25)
        let endPoint = CGPoint(x: frameWidth - 20, y: topDistance - 60)

        let path = UIBezierPath()
        path.move(to: startPoint)
        path.addQuadCurve(to: endPoint, controlPoint: controlPoint)

        let duration: Double = 0.8
        let animation = CAKeyframeAnimation(keyPath: "position")
        animation.path = path.cgPath
        animation.duration = duration + 0.1
        button.layer.add(animation, forKey: "animate along path")

        UIView.animate(withDuration: duration, animations: {
            button.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        }) { (_) in
            button.removeFromSuperview()
        }
    }

    // MARK: - UIImage methods
    static func resizeImage(image: UIImage, maxSide: CGFloat) -> UIImage {
        let size = image.size
        var newSize: CGSize

        if size.width > size.height && size.width > maxSide {
            let widthRatio = maxSide / size.width
            let height = size.height * widthRatio
            newSize = CGSize(width: maxSide, height: height)
        } else if size.height > size.width && size.height > maxSide {
            let heightRatio = maxSide / size.height
            let width = size.width * heightRatio
            newSize = CGSize(width: width, height: maxSide)
        } else {
            return image
        }

        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
}
