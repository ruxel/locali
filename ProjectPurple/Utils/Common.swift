//
//  Common.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/7/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

struct k {

    static let appName: String = Bundle.main.infoDictionary![kCFBundleNameKey as! String] as! String
    static let cornerRadius: CGFloat = 15
    static let cellHeight: CGFloat = 240

    struct firecloud {
        static let businesses           = "businesses"
        static let cart                 = "cart"
        static let categories           = "categories"
        static let comment              = "Comment"
        static let feedback             = "feedback"
        static let images               = "images"
        static let logUrl               = "Log url"
        static let profilePicture       = "profile_picture"
        static let recentSearches       = "recentSearches"
        static let requiredAppVersion   = "required_app_version"
        static let terms                = "terms"
        static let users                = "users"
    }

    struct notification {
        static let hideAddProductView     = Notification.Name("hideAddProductView")
        static let hideAllAddProductViews = Notification.Name("hideAllAddProductViews")
        static let userChanged            = Notification.Name("userChanged")
    }

    struct userData {
        static let publicData = "public"
        static let privateData = "private"
        static let firstName = "first_name"
        static let lastName = "last_name"
        static let email = "email"
        static let provider = "provider"
        static let profilePicture = "profile_picture"
        static let oneSignalId = "oneSignalId"
    }
}
