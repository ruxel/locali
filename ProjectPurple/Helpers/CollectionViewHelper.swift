//
//  CollectionViewHelper.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/22/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

struct CollectionViewHelper {

    static func getFlowLayoutForCategoriesCV() -> UICollectionViewFlowLayout {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 15, right: 10)
        flowLayout.minimumLineSpacing = 18
        return flowLayout
    }

    static func getCategoryCellSize(for view: UIView) -> CGSize {
        let paddingSpace: CGFloat = (10 * 2) + 20
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / 2

        return CGSize(width: widthPerItem, height: widthPerItem * 0.8)
    }
}
