//
//  ToastMessageHelper.swift
//  Hoowie
//
//  Created by Roger Pintó Diaz on 10/29/19.
//  Copyright © 2019 Roger Pintó Diaz. All rights reserved.
//

import Toast_Swift

enum ToastMessage {
    
    /// Error messages
    case emptyEmail
    case invalidEmail
    case resetPasswordError
    case tryAgain
    case uploadImageError
    case userNotFound
    
    /// Permissions
    case locationPermissionDeniedError
    case locationPermissionError
    case locationPermissionRestrictedError
    
    /// Other messages
    case feedbackIsEmpty
    case resetPasswordSuccess
    case deleteUserSuccess
    case noMoreStock
    case toDeleteTheProductUse

    /// Login
    case invalidPassword
    case passwordDontMatch
    case youNeedToLogIn
    case registerSuccess
    
    var stringValue: String {
        switch self {
            /// Error messages
            case .emptyEmail:
                return ls.email_is_empty_etc()

            case .invalidEmail:
                return ls.provide_a_valid_email()

            case .resetPasswordError:
                return ls.couldnt_reset_your_password()

            case .tryAgain:
                return ls.oops_something_went_wrong_please_try_again()

            case .uploadImageError:
                return ls.oops_we_couldnt_upload_the_image()

            case .userNotFound:
                return ls.email_address_is_not_registered_etc(k.appName)

            /// Permissions
            case .locationPermissionDeniedError:
                return ls.we_use_your_location_etc(k.appName)

            case .locationPermissionError:
                return ls.location_services_are_disabled_etc()

            case .locationPermissionRestrictedError:
                return ls.location_permission_is_restricted_etc(k.appName)

            /// Other messages
            case .feedbackIsEmpty:
                return ls.fill_the_feedback_form()

            case .resetPasswordSuccess:
                return ls.please_check_your_email_to_reset_your_password()

            case .deleteUserSuccess:
                return ls.user_deleted_successfully()

            case .noMoreStock:
                return ls.currentlyThereIsNoMoreStockForThisProduct()

            case .toDeleteTheProductUse:
                return ls.toDeleteTheProductUseTheRemoveButton()

            /// Login
            case .invalidPassword:
                return ls.password_must_be_etc()

            case .passwordDontMatch:
                return ls.passwords_dont_match_etc()

            case .youNeedToLogIn:
                return ls.youHaveToLogInToAddTheProductToYourCart()

            case .registerSuccess:
                return ls.userCreatedSuccessfully()
        }
    }
}

final class ToastMessageHelper {
    
    static func showToast(_ toastMessage: ToastMessage, duration: TimeInterval = 2, position: ToastPosition = .center) {
        showToastWithMessage(toastMessage.stringValue, duration: duration, position: position)
    }
    
    static func showToastWithMessage(_ message: String, duration: TimeInterval = 2, position: ToastPosition = .center) {
        Utils.topViewController().view.makeToast(message, duration: duration, position: position)
    }
}
