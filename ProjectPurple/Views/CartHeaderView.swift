//
//  CartHeaderView.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/25/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

final class CartHeaderView: CBAutoXibView {

    // MARK: - IBOutlets
    @IBOutlet weak var ivHeader: UIImageView!
    @IBOutlet weak var lblBusinessName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var vSeparatorLine: UIView!

    func setupView(business: Business, totalAmount: String) {
        ivHeader.circularView()
        ImageLoadHelper.load(business.imageUrl, in: ivHeader)
        lblBusinessName.text = business.name
        lblAmount.text = "€\(totalAmount)"
    }
}
