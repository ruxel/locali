//
//  CartItemCell.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/25/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

final class CartItemCell: UITableViewCell, UITextFieldDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var ivItem: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var tfQuantity: UITextField!
    @IBOutlet weak var btnRemove: UIButton!

    // MARK: - Properties
    private var cartItem: CartProduct?

    // MARK: - Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()

        btnRemove.text = ls.remove()
        tfQuantity.delegate = self
    }

    func setupCell(cartItem: CartProduct) {
        self.cartItem = cartItem

        ImageLoadHelper.load(cartItem.imageUrl, in: ivItem)
        lblName.text = cartItem.name
        lblPrice.text = ls.each("€", cartItem.price)

        if let price = Double(cartItem.price) {
            lblAmount.text = "€" + String(format: "%.2f", price * Double(cartItem.quantity))
        }
        tfQuantity.text = String(cartItem.quantity)
    }

    @IBAction func onRemove(_ sender: UIButton) {
        guard let cartItem = cartItem else {
            MyLogE("CartItemCell: cartItem is nil!")
            return
        }
        DataService.instance.removeProductFromCart(cartItem.productId)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let cartItem = cartItem else { return }

        if let text = textField.text, let newQuantity = Int(text), newQuantity != cartItem.quantity {
            if newQuantity == 0 {
                textField.text = String(cartItem.quantity)
                ToastMessageHelper.showToast(.toDeleteTheProductUse)
            } else {
                DataService.instance.updateCartProduct(cartItem, quantity: newQuantity)
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
