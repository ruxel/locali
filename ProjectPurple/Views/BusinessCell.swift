//
//  BusinessCell.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/7/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

final class BusinessCell: UICollectionViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var vContainer: UIView!
    @IBOutlet weak var ivHeader: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var lblAddressTwo: UILabel!
    @IBOutlet weak var btnBuy: UIButton!
    @IBOutlet weak var vRating: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        ivHeader.roundCorners(radius: k.cornerRadius)
        vContainer.roundCorners(radius: k.cornerRadius)
        vContainer.shadow()
        btnBuy.circularView()
        btnBuy.setTitle(ls.buy(), for: UIControl.State())
    }

    func setupCell(_ business: Business) {
        ImageLoadHelper.load(business.imageUrl, in: ivHeader)
        lblName.text = business.name
        lblAddress1.text = business.address
        lblAddressTwo.text = "\(business.city), \(business.postal)"
        vRating.rating = business.rating
    }
}
