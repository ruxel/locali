//
//  CategoryCell.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/10/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

final class CategoryCell: UICollectionViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var ivBackground: UIImageView!
    @IBOutlet weak var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        ivBackground.roundCorners(radius: k.cornerRadius)
    }

    func setupCell(_ category: FoodCategory) {
        ImageLoadHelper.load(category.imageUrl, in: ivBackground)
        lblName.text = category.name.capitalized
    }
}
