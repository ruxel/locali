//
//  ShopItemCell.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/20/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import FirebaseAuth
import UIKit

protocol ShopItemCellListener: class {
    func updateItem(_ item: Item?, quantity: Int)
    func updateAddViewStatus(visibleItemId: String?)
}

final class ShopItemCell: UICollectionViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var vAddProduct: UIView!
    @IBOutlet weak var ivHeader: UIImageView!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblExtraInfo: UILabel!

    // MARK: - Properties
    weak var delegate: ShopItemCellListener?
    var item: Item?
    var selectedQuantity: Int = 0

    // MARK: - Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()

        hideAddProductView()
        lblPrice.textColor = R.color.localiGreen()!
        NotificationCenter.default.addObserver(self, selector: #selector(hideAddProductViewWith(notification:)), name: k.notification.hideAddProductView, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideAddProductView), name: k.notification.hideAllAddProductViews, object: nil)
    }

    // MARK: - Public methods
    func setupCell(_ item: Item, delegate: ShopItemCellListener) {
        self.item = item
        self.delegate = delegate

        selectedQuantity = DataService.instance.userCart.first(where: { $0.productId == item.productId })?.quantity ?? 0
        ImageLoadHelper.load(item.imageUrl, in: ivHeader)
        lblName.text = item.name
        lblPrice.text = ls.each("€", item.price)
        lblExtraInfo.text = item.info
    }

    // MARK: - IBOutlets
    @IBAction func onAdd(_ sender: UIButton) {
        guard Auth.auth().currentUser != nil else {
            Utils.topViewController().present(SignInViewController.create(), animated: true) {
                ToastMessageHelper.showToast(.youNeedToLogIn)
            }
            return
        }
        delegate?.updateAddViewStatus(visibleItemId: item?.productId)
        increaseSelectedQuantity()
    }

    @IBAction func onRemove(_ sender: UIButton) {
        guard Auth.auth().currentUser != nil else {
            Utils.topViewController().present(SignInViewController.create(), animated: true) {
                ToastMessageHelper.showToast(.youNeedToLogIn)
            }
            return
        }
        decreaseSelectedQuantity()
    }

    // MARK: - Support methods
    private func showAddProductView() {
        if #available(iOS 13.0, *) {
            vAddProduct.backgroundColor = .secondarySystemGroupedBackground
        } else {
            vAddProduct.backgroundColor = .white
        }
        vAddProduct.roundCorners()
        vAddProduct.shadow()
        btnRemove.isHidden = false
        lblQuantity.isHidden = false
    }

    @objc private func hideAddProductViewWith(notification: NSNotification) {
        if let productId = notification.userInfo?["productId"] as? String, productId == item?.productId {
            hideAddProductView()
        }
    }

    @objc private func hideAddProductView() {
        vAddProduct.backgroundColor = .clear
        vAddProduct.layer.shadowOpacity = 0
        btnRemove.isHidden = true
        lblQuantity.isHidden = true
    }

    private func increaseSelectedQuantity() {
        guard let maxStock = item?.stock, selectedQuantity + 1 <= maxStock else {
            ToastMessageHelper.showToast(.noMoreStock)
            return
        }

        if selectedQuantity >= 1 {
            if #available(iOS 13.0, *) {
                btnRemove.setImage(UIImage(systemName: "minus.circle"), for: UIControl.State())
            }
        }
        showAddProductView()
        selectedQuantity += 1
        lblQuantity.text = String(selectedQuantity)

        delegate?.updateItem(item, quantity: selectedQuantity)
        Utils.performAddToCartAnimation()
    }

    private func decreaseSelectedQuantity() {
        selectedQuantity -= 1
        lblQuantity.text = String(selectedQuantity)

        if selectedQuantity == 0 {
            hideAddProductView()
        } else if selectedQuantity == 1 {
            if #available(iOS 13.0, *) {
                btnRemove.setImage(UIImage(systemName: "trash"), for: UIControl.State())
            }
        }

        delegate?.updateItem(item, quantity: selectedQuantity)
    }
}
