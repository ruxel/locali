//
//  BusinessAnnotation.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/18/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import MapKit
import UIKit

final class BusinessAnnotation: MKPointAnnotation {

    func getBusinessAnnotationView() -> UIView {
        let dotWidth: CGFloat = 18
        let blueDotView = UIView(frame: CGRect(x: 0, y: 0, width: dotWidth, height: dotWidth))
        blueDotView.backgroundColor = R.color.localiGreen()
        blueDotView.circularView()
        blueDotView.shadow()

        return blueDotView
    }
}
