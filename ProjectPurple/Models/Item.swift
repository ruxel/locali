//
//  Item.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/21/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import Foundation

struct Item: Decodable {

    var name: String
    var imageUrl: String
    var price: String
    var info: String
    var stock: Int
    var category: String
    var productId: String
}
