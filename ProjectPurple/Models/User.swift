//
//  User.swift
//  Bookie
//
//  Created by Roger Pintó Diaz on 1/26/19.
//  Copyright © 2019 Roger Pintó Diaz. All rights reserved.
//

import Foundation

struct User {
    
    var firstName: String
    var lastName: String
    var email: String
    var provider: String
    var profilePictureUrl: String
    
    init(firstName: String, lastName: String, email: String, provider: String, profilePictureUrl: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.provider = provider
        self.profilePictureUrl = profilePictureUrl
    }
    
    func toUserData() -> JSONObject {
        let privateData: JSONObject = [k.userData.email: email, k.userData.provider: provider]
        let publicData: JSONObject = [k.userData.firstName: firstName, k.userData.lastName: lastName, k.userData.profilePicture: profilePictureUrl]
        
        return [k.userData.privateData: privateData, k.userData.publicData: publicData]
    }
}
