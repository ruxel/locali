//
//  Business.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/7/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import Foundation
import MapKit

struct Business: Decodable {

    var name: String
    var address: String
    var imageUrl: String
    var rating: Double
    var city: String
    var postal: String
    var availability: String
    var delivery: String
    var pickup: String
    var latitude: String
    var longitude: String
    var id: String
    var categories: [String]

    var coordinate: CLLocationCoordinate2D? {
        get {
            guard let latitude = Double(latitude), let longitude = Double(longitude) else {
                return nil
            }
            return CLLocationCoordinate2DMake(latitude, longitude)
        }
    }

    var hasDelivery: Bool {
        delivery == "1"
    }

    var hasPickup: Bool {
        pickup == "1"
    }
}
