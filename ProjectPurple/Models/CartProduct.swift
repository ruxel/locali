//
//  CartProduct.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/24/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import Foundation

struct CartProduct: Decodable {

    var businessId: String
    var productId: String
    var quantity: Int
    var name: String
    var imageUrl: String
    var price: String
}
