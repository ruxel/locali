//
//  FoodCategory.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/10/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import Foundation

struct FoodCategory: Decodable {

    var name: String
    var imageUrl: String
    var id: String
}
