//
//  DataService.swift
//  Bookie
//
//  Created by Roger Pintó Diaz on 7/14/18.
//  Copyright © 2018 Roger Pintó Diaz. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

protocol UserCartListener: class {
    func userCartUpdated()
}

final class DataService {

    // MARK: - Var and const
    static let instance = DataService()

    private var _REF_BASE = Firestore.firestore()
    private var _REF_STORAGE = Storage.storage().reference()

    var REF_BASE: Firestore {
        let settings = _REF_BASE.settings
        _REF_BASE.settings = settings

        return _REF_BASE
    }

    var REF_STORAGE: StorageReference {
        _REF_STORAGE
    }

    var foodCategories: [FoodCategory] {
        _foodCategories
    }

    private var _foodCategories = [FoodCategory]()

    private var currentUserId: String? {
        Auth.auth().currentUser?.uid
    }

    private var userCartReference : CollectionReference? {
        guard let currentUser = currentUserId else {
            MyLogE("userCartReference: User can't be nil!")
            return nil
        }
        return REF_BASE.collection(k.firecloud.users).document(currentUser).collection(k.firecloud.cart)
    }

    var userCart: [CartProduct] {
        privateUserCart
    }

    private var privateUserCart = [CartProduct]()

    weak var cartViewControllerDelegate: CartViewController?

    // MARK: - User Data
    func createDBUser(uid: String, user: User, onCompletion: @escaping () -> ()) {
        REF_BASE.collection(k.firecloud.users).document(uid).setData(user.toUserData(), completion: { (err) in
            guard err == nil else {
                MyLogE(err.debugDescription)
                return
            }

            onCompletion()
        })
    }

    func uploadProfilePicture(photo: UIImage, onCompletion: @escaping () -> ()) {
        guard let currentUser = currentUserId else { return }

        let image = Utils.resizeImage(image: photo, maxSide: 400).jpegData(compressionQuality: 0.7)
        let imageRef = DataService.instance.REF_STORAGE.child(k.firecloud.images).child(currentUser).child(UUID().uuidString)

        imageRef.putData(image!, metadata: nil) { (metadata, error) in
            if let error = error {
                self.showFirestoreUploadPhotoError(error: error)
            } else {
                MyLogI("Firestore: UploadPhoto: Image uploaded successfully")
                imageRef.downloadURL(completion: { (url, error) in
                    if let url = url {
                        MyLogI("Firestore: Image url downloaded successfully")
                        self.REF_BASE.collection(k.firecloud.users).document(currentUser).updateData([k.firecloud.profilePicture: url.absoluteString])
//                        imageCache.setObject(photo, forKey: url as AnyObject)
                        NotificationCenter.default.post(name: k.notification.userChanged, object: nil) //FixMe - Remove when passing image between views
                        onCompletion()
                    } else {
                        self.showFirestoreUploadPhotoError(error: error)
                    }
                })
            }
        }
    }

    func getProfilePicture(onCompletion: @escaping (String) -> ()) {
        guard let currentUser = currentUserId else { return }
        MyLogI("FireBase: Getting user profile picture")

        REF_BASE.collection(k.firecloud.users).document(currentUser).getDocument { (snapshot, err) in
            let profilePictureUrl = snapshot?.data()?[k.userData.profilePicture] as? String ?? ""
            MyLogI("FireBase: User profile picture: \(profilePictureUrl)")
            onCompletion(profilePictureUrl)
        }
    }

    private func showFirestoreUploadPhotoError(error: Error?) {
        MyLogE("Firestore: UploadPhoto: \(error.debugDescription)")
        Utils.topViewController().showOkAlertWithMessage(title: ls.oops_we_couldnt_upload_the_image())
    }

    // MARK: - Firestore: User Cart
    func addProductToCart(_ item: Item, businessId: String, quantity: Int) {
        if quantity == 0 {
            removeProductFromCart(item.productId)
        } else {
            let productDict: JSONObject = [
                "businessId": businessId,
                "imageUrl": item.imageUrl,
                "productId": item.productId,
                "name": item.name,
                "price": item.price,
                "quantity": quantity
            ]
            userCartReference?.document(item.productId).setData(productDict) { (error) in
                if let error = error {
                    MyLogE("addProductToCart: Update: \(error)")
                }
            }
        }
    }

    func updateCartProduct(_ cartProduct: CartProduct, quantity: Int) {
        let productDict: JSONObject = [
            "businessId": cartProduct.businessId,
            "imageUrl": cartProduct.imageUrl,
            "productId": cartProduct.productId,
            "name": cartProduct.name,
            "price": cartProduct.price,
            "quantity": quantity
        ]
        userCartReference?.document(cartProduct.productId).setData(productDict) { (error) in
            if let error = error {
                MyLogE("addProductToCart: Update: \(error)")
            }
        }
    }

    func removeProductFromCart(_ productId: String) {
        userCartReference?.document(productId).delete { (error) in
            if let error = error {
                MyLogE("addProductToCart: Delete: \(error)")
            }
        }
    }

    func setupSharedUserCartInstance() {
        userCartReference?.addSnapshotListener({ (querySnapshot, error) in
            if let error = error {
                MyLogE("fetchCart: \(error)")
                return
            }

            guard let snapshot = querySnapshot else { return }

            do {
                let cartProducts: [CartProduct] = try snapshot.decoded()
                self.privateUserCart.removeAll()
                self.privateUserCart.append(contentsOf: cartProducts)
                self.cartViewControllerDelegate?.userCartUpdated()
            } catch {
                MyLogE("Couldn't decode [CartProduct]")
            }
        })
    }

    // MARK: - Firestore: Feedback
    func sendFeedback(text: String, onCompletion: @escaping () -> ()) {
        let user = currentUserId ?? UUID().uuidString
        REF_BASE.collection(k.firecloud.feedback).document(user).collection(k.firecloud.feedback).addDocument(data: [k.firecloud.comment: text, k.firecloud.logUrl: "no url"])
        onCompletion()
    }

    // MARK: - Firestore: Businesses
    func getBusinesses(onCompletion: @escaping ([Business]) -> ()) {
        REF_BASE.collection(k.firecloud.businesses).getDocuments(completion: { (querySnapshot, error) in
            guard let snapshot = querySnapshot else { return }

            do {
                let businesses: [Business] = try snapshot.decoded()
                return onCompletion(businesses)
            } catch {
                MyLogE("Couldn't decode business")
            }
        })
    }

    // MARK: - Firestore: Categories
    func getFoodCategories(onCompletion: @escaping ([FoodCategory]) -> ()) {
        REF_BASE.collection(k.firecloud.categories).getDocuments(completion: { (querySnapshot, error) in
            guard let snapshot = querySnapshot else { return }

            do {
                let foodCategories: [FoodCategory] = try snapshot.decoded()

                self._foodCategories.removeAll()
                self._foodCategories.append(contentsOf: foodCategories)

                return onCompletion(foodCategories)
            } catch {
                MyLogE("Couldn't decode food categories")
            }
        })
    }

    func getFoodCategoriesForBusinessID(_ businessID: String, onCompletion: @escaping ([FoodCategory]) -> ()) {
        REF_BASE.collection(k.firecloud.businesses).document(businessID).collection(k.firecloud.categories).getDocuments(completion: { (querySnapshot, error) in
            guard let snapshot = querySnapshot else { return }

            var businessFoodCategories = [FoodCategory]()

            for document in snapshot.documents { //FixMe - Avoid too many looping iterations
                for foodCategory in self.foodCategories {
                    if foodCategory.name == document.documentID {
                        businessFoodCategories.append(foodCategory)
                    }
                }
            }

            return onCompletion(businessFoodCategories)
        })
    }

    func getItemsFor(businessID: String, onCompletion: @escaping ([Item]) -> ()) {
        REF_BASE.collection(k.firecloud.businesses).document(businessID).collection(k.firecloud.categories).getDocuments { (querySnapshot, error) in
            guard let snapshot = querySnapshot else { return }

            do {
                var items = [Item]()

                for document in snapshot.documents {
                    let item: Item = try document.decoded()
                    items.append(item)
                }

                return onCompletion(items)
            } catch {
                MyLogE("Couldn't decode items for \(businessID)")
            }
        }
    }

    // MARK: - App version
    func checkIfAppUpdateIsRequired(onCompletion: @escaping () -> Void) {
        guard let info = Bundle.main.infoDictionary, let currentVersion = info["CFBundleShortVersionString"] as? String else {
            MyLogE("AppUpdate: Invalid bundle info")
            return
        }
        MyLogI("AppUpdate: Current version: \(currentVersion)")

        REF_BASE.collection(k.firecloud.requiredAppVersion).document(k.firecloud.requiredAppVersion).getDocument(completion: { (snapshot, err) in
            guard let snapshotData = snapshot?.data() else { return }

            if let requiredAppVersion = snapshotData[k.firecloud.requiredAppVersion] as? String {
                if currentVersion.toAppVersion() < requiredAppVersion.toAppVersion() {
                    onCompletion()
                }
            }
        })
    }

    // MARK: - Terms and conditions
    func getTermsUrl(onCompletion: @escaping (String) -> Void) {
        REF_BASE.collection(k.firecloud.terms).document(k.firecloud.terms).getDocument(completion: { (snapshot, err) in
            guard let snapshotData = snapshot?.data() else { return }

            if let termsUrl = snapshotData["url"] as? String {
                onCompletion(termsUrl)
            }
        })
    }
}
