//
//  AuthService.swift
//  Hoowie
//
//  Created by Roger Pintó Diaz on 7/14/18.
//  Copyright © 2018 Roger Pintó Diaz. All rights reserved.
//

import AuthenticationServices
import Foundation
import Firebase
import FirebaseAuth

final class AuthService {

    static let instance = AuthService()

    func registerUser(email: String, password: String, onCompletion: @escaping (_ status: Bool, _ error: Error?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            guard let user = user else {
                onCompletion(false, error)
                return
            }

            let firebaseUser = User(firstName: "", lastName: "", email: user.user.email!, provider: user.user.providerID, profilePictureUrl: "")
            DataService.instance.createDBUser(uid: user.user.uid, user: firebaseUser, onCompletion: {}) //FixMe - Handle errors in createDBUser
            onCompletion(true, nil)
        }
    }

    func loginUser(email: String, password: String, onCompletion: @escaping (_ status: Bool, _ error: Error?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let error = error {
                onCompletion(false, error)
                return
            }

            NotificationCenter.default.post(name: k.notification.userChanged, object: nil)
            onCompletion(true, nil)
        }
    }

    func sendPasswordReset(email: String) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            guard error == nil else {
                MyLogE("SignIn: Reset password. \(String(describing: error?.localizedDescription))")
//                Utils.topViewController().showOkAlertWithMessage(title: ls.couldnt_reset_your_password())
                return
            }
//            Utils.topViewController().showOkAlertWithMessage(title: ls.please_check_your_email_to_reset_your_password())
        }
    }

    @available(iOS 13.0, *)
    func signInWithApple(authorization: ASAuthorization, nonce: String?, onCompletion: @escaping () -> Void) {
        guard let nonce = nonce else {
            fatalError("Invalid state: A login callback was received, but no login request was sent.")
        }

        guard let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential,
            let appleIDToken = appleIDCredential.identityToken else {

                MyLogE("Unable to fetch identity token")
                return
        }

        guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
            MyLogE("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
            return
        }

        // Initialize a Firebase credential
        let credential = OAuthProvider.credential(withProviderID: "apple.com", idToken: idTokenString, rawNonce: nonce)

        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                // Error. If error.code == .MissingOrInvalidNonce, make sure
                // you're sending the SHA256-hashed nonce as a hex string with
                // your request to Apple.
                MyLogE("AuthService: signInWithApple: \(error.localizedDescription)")
                return
            }

            if let authResult = authResult {
                let userName = appleIDCredential.fullName?.givenName ?? "Unknown name"
                let userFamilyName = appleIDCredential.fullName?.familyName ?? ""
                let email = appleIDCredential.email ?? "Unknown email"
                let providerID = authResult.user.providerID

                let firebaseUser = User(firstName: userName, lastName: userFamilyName, email: email, provider: providerID, profilePictureUrl: "")
                DataService.instance.createDBUser(uid: authResult.user.uid, user: firebaseUser, onCompletion: {})
                //FixMe - Handle errors in createDBUser
            }

            MyLogI("AuthService: SignInWithApple: User logged in successfully")
            NotificationCenter.default.post(name: k.notification.userChanged, object: nil)
            onCompletion()
        }
    }
}
