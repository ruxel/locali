//
//  CartViewController.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/25/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import FirebaseFirestore
import UIKit

final class CartViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UserCartListener {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnCheckout: UIButton!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!

    // MARK: - Properties
    private let headerHeight: CGFloat = 80
    private var cartProducts = [CartProduct]()
    private var businessIds = [String]()
    private var itemsPerBusiness = [Int]()
    private var businesses = [Business]()

    // MARK: - Public methods
    static func create() -> UIViewController {
        let viewController = R.storyboard.cart().instantiateViewController(withIdentifier: "CartVC") as! CartViewController
        viewController.modalPresentationStyle = .fullScreen
        return viewController
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        btnCheckout.setTitle(ls.checkout(), for: UIControl.State())
        lblTitle.text = ls.myBasket()
        setupTableView()
        setupCheckout()
        refreshCart()
        DataService.instance.cartViewControllerDelegate = self
    }

    deinit {
        MyLogD("Dealloc: \(self)")
    }

    // MARK: - UI methods
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "CartItemCell", bundle: nil), forCellReuseIdentifier: "CartItemCell")
        tableView.tableFooterView = UIView()
        tableView.separatorColor = R.color.localiGreen()!
    }

    private func setupCheckout() {
        btnCheckout.circularView()
    }

    private func updateCheckoutAmount() {
        var total: Double = 0
        cartProducts.forEach { (product) in
            if let price = Double(product.price) {
                total += price * Double(product.quantity)
            }
        }

        onMain {
            self.lblAmount.text = "€" + String(format: "%.2f", total)
        }
    }

    private func businessAmount(forSection section: Int) -> String {
        var total: Double = 0
        cartProducts.forEach { (product) in
            if businesses[section].id == product.businessId {
                if let price = Double(product.price) {
                    total += price * Double(product.quantity)
                }
            }
        }

        return String(format: "%.2f", total)
    }

    // MARK: - Fetch data
    private func clearArrays() {
        businesses.removeAll()
        businessIds.removeAll()
        itemsPerBusiness.removeAll()
        cartProducts.removeAll()
    }

    private func refreshCart() {
        clearArrays()

        let newCartProducts = DataService.instance.userCart
        cartProducts.append(contentsOf: newCartProducts)
        cartProducts.sort { $0.businessId < $1.businessId }
        updateCheckoutAmount()

        for product in cartProducts {
            let businessId = product.businessId

            if !businessIds.contains(businessId) {
                businessIds.append(businessId)
            }
        }

        businessIds.sort()

        for businessId in businessIds {
            itemsPerBusiness.append(cartProducts.filter { $0.businessId == businessId }.count)
        }

        fetchBusinesses()
    }

    private func fetchBusinesses() {
        DataService.instance.getBusinesses { (businesses) in
            for businessId in self.businessIds {
                if let business = businesses.filter({ $0.id == businessId }).first {
                    self.businesses.append(business)
                }
            }
            self.businesses.sort { $0.id < $1.id }

            onMain {
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - UserCartListener
    func userCartUpdated() {
        refreshCart()
    }
    
    // MARK: - onButton actions
    @IBAction func onBack(_ sender: UIButton) {
        dismiss(animated: true)
    }

    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        itemsPerBusiness[section]
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartItemCell", for: indexPath) as! CartItemCell

        if indexPath.section == 0 {
            cell.setupCell(cartItem: cartProducts[indexPath.row])
        } else {
            let index = indexPath.section * itemsPerBusiness[indexPath.section - 1] + indexPath.row
            cell.setupCell(cartItem: cartProducts[index])
        }
        return cell
    }

    // MARK: - UITableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        itemsPerBusiness.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard !businesses.isEmpty else { return nil }

        let headerView = CartHeaderView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: headerHeight))
        headerView.setupView(business: businesses[section], totalAmount: businessAmount(forSection: section))
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        headerHeight
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        135
    }

    // MARK: - UIScrollViewDelegate
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}
