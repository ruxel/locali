//
//  SingleWebViewViewController.swift
//  Locali.
//
//  Created by Pinto Diaz, Roger on 4/27/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit
import WebKit

final class SingleWebViewViewController: UIViewController, WKNavigationDelegate {

    // MARK: - Var and const
    let webView = WKWebView()
    let activityIndicator = UIActivityIndicatorView()
    var url: String!

    // MARK: - Public Interface
    static func create(url: String, title: String) -> UIViewController {
        let viewController = SingleWebViewViewController()
        viewController.url = url
        viewController.title = title
        return viewController
    }

    // MARK: - ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupWebView()
        loadWebView()
        setupActivityIndicator()
    }

    deinit {
        MyLogD("SingleWebViewVC dealloc")
    }

    // MARK: - UI methods
    func setupWebView() {
        webView.navigationDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(webView)

        webView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        webView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        webView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    }

    func loadWebView() {
        if let url = URL(string: url) {
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }

    func setupActivityIndicator() {
        activityIndicator.style = .gray
        activityIndicator.hidesWhenStopped = true
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(activityIndicator)

        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true

        activityIndicator.startAnimating()
    }

    // MARK: - WKNavigationDelegate
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        guard !webView.isLoading else { return }

        MyLogI("SingleWebView: Finished loading webView")
        activityIndicator.stopAnimating()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        MyLogE("SingleWebView: WebView didFailLoadWithError \(error.localizedDescription)")
    }
}
