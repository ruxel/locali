//
//  SearchProductsViewController.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/26/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import FirebaseFirestore
import UIKit

final class SearchProductsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, ShopItemCellListener {

    // MARK: - IBOutlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnBack: UIButton!

    // MARK: - Properties
    private var businesses = [Business]()
    private var itemsDict = [String: [Item]]()
    private var items = [Item]()
    private var filteredItems = [Item]()
    private var visibleItemId: String?
    private var timer: Timer?
    private var lastSearch = ""

    // MARK: - Public interface
    static func create(businesses: [Business]) -> UIViewController {
        let storyboard = R.storyboard.searchProducts()
        let viewController = storyboard.instantiateViewController(withIdentifier: "SearchProductsVC") as! SearchProductsViewController
        viewController.businesses = businesses
        return viewController
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = ls.findEverythingYouNeed()
        btnBack.tintedImage(image: R.image.backArrow()!, color: R.color.localiGreen()!)
        searchBar.placeholder = ls.what_do_you_need_q()
        searchBar.delegate = self

        setupCollectionView()
        fetchData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        searchBar.becomeFirstResponder()
    }

    deinit {
        MyLogD("Dealloc: \(self)")
    }

    // MARK: - UI methods
    private func setupCollectionView() {
        collectionView.register(UINib(nibName: "ShopItemCell", bundle: nil), forCellWithReuseIdentifier: "ShopItemCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.keyboardDismissMode = .onDrag

        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 15, right: 5)
        flowLayout.minimumLineSpacing = 5
        collectionView.collectionViewLayout = flowLayout
    }

    private func clearArrays() {
        items.removeAll()
        filteredItems.removeAll()
    }

    private func fetchData() {
        clearArrays()

        for businessId in businesses.map({ $0.id }) {
            DataService.instance.getItemsFor(businessID: businessId) { (newItems) in
                self.itemsDict[businessId] = newItems
                self.items.append(contentsOf: newItems)
                self.filteredItems.append(contentsOf: newItems)

                self.items.sort { $0.name < $1.name }
                self.filteredItems.sort { $0.name < $1.name }

                onMain {
                    self.collectionView.reloadData()
                }
            }
        }
    }

    @IBAction func onBack(_ sender: UIButton) {
        dismiss(animated: true)
    }

    private func hideAddProductView() {
        guard visibleItemId != nil else {
            return
        }
        NotificationCenter.default.post(name: k.notification.hideAddProductView, object: nil, userInfo: ["productId": visibleItemId as Any])
    }

    // MARK: - Support methods
    private func stopTimer() {
        if timer != nil {
            timer?.invalidate()
        }
    }

    @objc private func updateSearchResults() {
        stopTimer()

        guard let searchText = searchBar.text, searchText.count > 2, searchText != lastSearch else {
            filteredItems.removeAll()
            filteredItems.append(contentsOf: items)
            collectionView.reloadData()
            return
        }

        MyLogD("ShopVC: Searching: \(searchText)")

        filteredItems.removeAll()
        filteredItems = items.filter({ (item) -> Bool in
            if item.category.contains(searchText) {
                return true

            } else if item.name.contains(searchText) {
                return true

            } else if item.info.contains(searchText) {
                return true

            } else {
                return false
            }
        })

        lastSearch = searchText
        collectionView.reloadData()
    }

    // MARK: - UIScrollViewDelegate
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        hideAddProductView()
    }

    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        filteredItems.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShopItemCell", for: indexPath) as? ShopItemCell else {
            fatalError()
        }
        cell.setupCell(filteredItems[indexPath.row], delegate: self)
        return cell
    }

    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        hideAddProductView()
    }

    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace: CGFloat = (10 * 2)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / 2

        return CGSize(width: widthPerItem, height: widthPerItem * 1.2)
    }

    // MARK: - ShopItemCellListener
    func updateItem(_ item: Item?, quantity: Int) {
        guard let item = item else {
            MyLogE("Couldn't update product: Item is nil")
            return
        }

        if let businessId = itemsDict.first(where: { $0.value.contains { $0.productId == item.productId } })?.key {
            DataService.instance.addProductToCart(item, businessId: businessId, quantity: quantity)
        }
    }

    func updateAddViewStatus(visibleItemId: String?) {
        guard let newVisibleItemId = visibleItemId else {
            return
        }

        if self.visibleItemId == nil {
            self.visibleItemId = newVisibleItemId
        } else if newVisibleItemId != self.visibleItemId {
            let userInfo = ["productId": self.visibleItemId as Any]
            NotificationCenter.default.post(name: k.notification.hideAddProductView, object: nil, userInfo: userInfo)
            self.visibleItemId = newVisibleItemId
        }
    }

    // MARK: - UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NotificationCenter.default.post(name: k.notification.hideAllAddProductViews, object: nil, userInfo: nil)
        stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateSearchResults), userInfo: nil, repeats: true)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
