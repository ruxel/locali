//
//  UserProfileViewController.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/18/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import FirebaseAuth
import UIKit

final class UserProfileViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserAddressOne: UILabel!

    // MARK: - Properties
    let cellTitles: [String] = ["", ls.nameAndAddress(), ls.security(), ls.termsAndConditions(), ls.notifications(), ls.logout()]

    // MARK: - Public interface
    static func create() -> UIViewController {
        UIStoryboard(name: "UserProfile", bundle: nil).instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileViewController
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()

        setupView()
    }

    // MARK: - onButton actions
    @IBAction func onBack(_ sender: UIButton) {
        dismiss(animated: true)
    }

    // MARK: - UI methods
    private func setupView() {
        lblUserAddressOne.text = "Santiago Rusinol 20\nCabrianes 08650"
        btnBack.tintedImage(image: R.image.backArrow()!, color: R.color.localiGreen()!)
    }

    private func logout() {
        let alert = UIAlertController(title: ls.logout(), message: ls.are_you_sure_you_want_to_logout_q(), preferredStyle: .actionSheet)
        let logoutAction = UIAlertAction(title: ls.logout(), style: .destructive) { (action) in
            do {
                try Auth.auth().signOut()
                self.dismiss(animated: true)
            } catch {
                MyLogE(error.localizedDescription)
            }
        }
        alert.addAction(logoutAction)

        let cancelAction = UIAlertAction(title: ls.cancel(), style: .cancel) { (action) in
            MyLogD("Settings: Logout canceled")
        }
        alert.addAction(cancelAction)

        present(alert, animated: true)
    }
}

// MARK: - UITableViewDataSource
extension UserProfileViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cellTitles.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            return UITableViewCell()
        }

        var cell = tableView.dequeueReusableCell(withIdentifier: "UserProfileCell")

        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "UserProfileCell")
        }

        cell?.accessoryType = .disclosureIndicator
        cell?.textLabel?.text = cellTitles[indexPath.row]

        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        indexPath.row == 0 ? 0.5 : 55
    }
}

// MARK: - UITableViewDelegate
extension UserProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if indexPath.row == 3 {
            DataService.instance.getTermsUrl { (termsUrl) in
                self.present(SingleWebViewViewController.create(url: termsUrl, title: ls.termsAndConditions()), animated: true)
            }
        } else if indexPath.row == 5 {
            logout()
        }
    }
}
