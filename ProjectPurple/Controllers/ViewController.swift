//
//  ViewController.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/5/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import FirebaseAuth
import MapKit
import UIKit

final class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MKMapViewDelegate, CLLocationManagerDelegate, AddressSelectorListener, UISearchBarDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnPostalCode: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnShowAll: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!

    // MARK: - Properties
    private let locationManager = CLLocationManager()
    private var lastLocation: CLLocation?
    private let regionRadius: CLLocationDistance = 700
    private var businesses = [Business]()
    private var filteredBusinesses = [Business]()
    private var foodCategories = [FoodCategory]()
    private var postalCode = ""
    private var selectedCoordinate: CLLocationCoordinate2D?

    // MARK: - Public interface
    static func create() -> UIViewController {
        UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainVC") as! ViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupCollectionView()
        setupMapView()
        setupLocationServices()
        setupSearchBar()
        setupButtons()
        setupSegmentedControl()
        updateButtonsText()

        fetchBusinessesData()
        fetchFoodCategoriesData()

        if checkLocationPermission() {
            locationManager.startUpdatingLocation()
        } else {
            getCoordinate(for: postalCode) { (location, error) in
                self.centerMap(in: location)
            }
        }

        DataService.instance.setupSharedUserCartInstance()
    }

    // MARK: - UI methods
    private func setupCollectionView() {
        collectionView.register(UINib(nibName: "CategoryCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.collectionViewLayout = CollectionViewHelper.getFlowLayoutForCategoriesCV()
    }

    private func setupMapView() {
        mapView.delegate = self
        mapView.showsPointsOfInterest = false
        mapView.showsCompass = true
        mapView.showsTraffic = false
        mapView.showsBuildings = false
        mapView.showsUserLocation = true
        mapView.roundCorners(radius: k.cornerRadius)
    }

    private func centerMap(in location: CLLocationCoordinate2D) {
        guard -90 <= location.latitude && location.latitude <= 90 else { return }

        onMain {
            let coordinateRegion = MKCoordinateRegion(center: location, latitudinalMeters: self.regionRadius, longitudinalMeters: self.regionRadius)
            self.mapView.setRegion(coordinateRegion, animated: true)
        }
    }

    private func setupSearchBar() {
        searchBar.placeholder = ls.what_do_you_need_q()
        searchBar.delegate = self
    }

    private func updateButtonsText() {
        let attributedString = NSMutableAttributedString()
            .normal(ls.showAllAround())
            .bold(postalCode)
        btnShowAll.setAttributedTitle(attributedString, for: UIControl.State())

        btnPostalCode.setTitle(postalCode, for: UIControl.State())
    }

    private func setupButtons() {
        btnShowAll.tintColor = .white
        btnShowAll.roundCorners(radius: k.cornerRadius)
        btnLocation.roundCorners(radius: k.cornerRadius)
        btnPostalCode.roundCorners(radius: k.cornerRadius)
    }

    private func setupSegmentedControl() {
        segmentedControl.setTitle(ls.all(), forSegmentAt: 0)
        segmentedControl.setTitle(ls.delivery(), forSegmentAt: 1)
        segmentedControl.setTitle(ls.pick_up(), forSegmentAt: 2)
    }

    private func refreshMapView() {
        mapView.removeAnnotations(mapView.annotations)

        for business in filteredBusinesses {
            if let coordinate = business.coordinate {
                let annotation = BusinessAnnotation()
                annotation.coordinate = coordinate
                annotation.title = business.name
                mapView.addAnnotation(annotation)
            }
        }
    }

    // MARK: - DataService methods
    private func fetchBusinessesData() {
        DataService.instance.getBusinesses { (businesses) in
            onMain {
                self.businesses.removeAll()
                self.businesses.append(contentsOf: businesses)
                self.filteredBusinesses.removeAll()
                self.filteredBusinesses.append(contentsOf: businesses)
                self.refreshMapView()
            }
        }
    }

    private func fetchFoodCategoriesData() {
        DataService.instance.getFoodCategories { (foodCategories) in
            onMain {
                self.foodCategories.removeAll()
                self.foodCategories.append(contentsOf: foodCategories)
                self.collectionView.reloadData()
            }
        }
    }

    // MARK: - Authorizations
    private func setupLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
        } else {
            ToastMessageHelper.showToast(.locationPermissionError)
        }
    }

    // MARK: - onButton actions
    @IBAction func onCart(_ sender: UIButton) {
        if Auth.auth().currentUser?.uid == nil {
            present(SignInViewController.create(), animated: true)
        } else {
            present(CartViewController.create(), animated: true)
        }
    }

    @IBAction func onProfile(_ sender: UIButton) {
        if Auth.auth().currentUser?.uid == nil {
            present(SignInViewController.create(), animated: true)
        } else {
            present(UserProfileViewController.create(), animated: true)
        }
    }

    @IBAction func onShowAll(_ sender: Any) {
        present(BusinessesViewController.create(), animated: true)
    }

    @IBAction func onPostalCode(_ sender: UIButton) {
        present(AddressSelectorViewController.create(withDelegate: self), animated: true)
    }

    @IBAction func onLocation(_ sender: UIButton) {
        if checkLocationPermission() {
            selectedCoordinate = nil
            locationManager.startUpdatingLocation()

            if let lastLocation = lastLocation {
                centerMap(in: lastLocation.coordinate)
            }
        }
    }

    @IBAction func onSegmentValueChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            filteredBusinesses = businesses

        } else if sender.selectedSegmentIndex == 1 {
            filteredBusinesses = businesses.filter { $0.hasDelivery }

        } else if sender.selectedSegmentIndex == 2 {
            filteredBusinesses = businesses.filter { $0.hasPickup }
        }

        refreshMapView()
    }

    // MARK: - Support methods
    private func getCoordinate(for postalCode: String, completionHandler: @escaping(CLLocationCoordinate2D, NSError?) -> Void ) {
        CLGeocoder().geocodeAddressString(postalCode) { (placemarks, error) in
            if error == nil {
                if let placemark = placemarks?.first, let location = placemark.location {
                    completionHandler(location.coordinate, nil)
                    return
                }
            }

            completionHandler(kCLLocationCoordinate2DInvalid, error as NSError?)
        }
    }

    private func updatePostalCode(for location: CLLocation) {
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            if let postalCode = placemarks?.first?.postalCode {
                onMain {
                    self.postalCode = postalCode
                    self.updateButtonsText()
                }
            }
        }
    }

    private func checkLocationPermission() -> Bool {
        let status = CLLocationManager.authorizationStatus()
        switch status {
            case .authorizedWhenInUse:
                return true

            case .denied:
                showGoToSettingsAlertWithMessage(ToastMessage.locationPermissionDeniedError.stringValue)
                return false

            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
                return false

            case .restricted:
                showGoToSettingsAlertWithMessage(ToastMessage.locationPermissionRestrictedError.stringValue)
                return false

            default:
                return false
        }
    }

    private func getDecimals(from cardinalString: Double) -> String {
        if let decimals = String(cardinalString).split(separator: ".").last {
            if decimals.count > 3 {
                return String(decimals.prefix(3))
            } else {
                return String(decimals)
            }
        }
        return ""
    }

    // MARK: - UISearchBarDelegate
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        present(SearchProductsViewController.create(businesses: businesses), animated: true)
        return false
    }

    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        foodCategories.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as? CategoryCell else {
            fatalError()
        }

        cell.setupCell(foodCategories[indexPath.row])
        return cell
    }

    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        present(BusinessesViewController.create(withCategoryFilter: foodCategories[indexPath.row]), animated: true)
    }

    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CollectionViewHelper.getCategoryCellSize(for: view)
    }

    // MARK: - AddressSelectorListener
    func didSelectMapItem(_ mapItem: MKMapItem) {
        guard let location = mapItem.placemark.location else { return }

        selectedCoordinate = location.coordinate
        updatePostalCode(for: location)
        centerMap(in: location.coordinate)

        //FixMe Handle selected location
        let annotation = MKPointAnnotation()
        annotation.coordinate = location.coordinate
        annotation.title = ls.selectedLocation()
        mapView.addAnnotation(annotation)
    }

    // MARK: - MKMapViewDelegate
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationIdentifier = ""
        if annotation is MKUserLocation {
            annotationIdentifier = "UserLocation"

        } else if annotation is BusinessAnnotation {
            annotationIdentifier = "BusinessAnnotation"

        } else {
            annotationIdentifier = "PointOfInterest"
        }

        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)

        if !(annotation is MKUserLocation) {
            if let businessAnnotation = annotation as? BusinessAnnotation {
                if annotationView == nil {
                    annotationView = MKAnnotationView(annotation: businessAnnotation, reuseIdentifier: annotationIdentifier)

                    let businessAnnotationView = businessAnnotation.getBusinessAnnotationView()
                    businessAnnotationView.center = annotationView!.frame.origin
                    annotationView?.addSubview(businessAnnotationView)

                } else {
                    annotationView?.annotation = businessAnnotation
                }
            } else {
                if annotationView == nil {
                    annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
                } else {
                    annotationView?.annotation = annotation
                }
            }
            annotationView?.canShowCallout = true
        }

        return annotationView
    }

    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            MyLogI("Location: .authorizedWhenInUse")

            if let coordinate = locationManager.location?.coordinate {
                centerMap(in: coordinate)
            } else {
                _ = checkLocationPermission()
            }

        } else if CLLocationManager.authorizationStatus() == .denied {
            MyLogE("Location: .denied")
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let newLocation = locations.last else { return }

        let latitude = newLocation.coordinate.latitude
        let longitude = newLocation.coordinate.longitude

        if newLocation != lastLocation && latitude != 0 && longitude != 0 {
            if let lastLocation = lastLocation {
                let roundedNewLatitude = getDecimals(from: latitude)
                let roundedNewLongitude = getDecimals(from: longitude)
                let roundedLastLatitude = getDecimals(from: lastLocation.coordinate.latitude)
                let roundedLastLongitude = getDecimals(from: lastLocation.coordinate.longitude)

                if roundedNewLatitude != roundedLastLatitude && roundedNewLongitude != roundedLastLongitude {
                    centerMap(in: newLocation.coordinate)
                }
            }

            lastLocation = newLocation

            if selectedCoordinate == nil {
                updatePostalCode(for: newLocation)
            }
        }
    }
}
