//
//  UpdateAppViewController.swift
//  Hoowie
//
//  Created by Roger Pintó Diaz on 10/29/18.
//  Copyright © 2018 Roger Pintó Diaz. All rights reserved.
//

import UIKit

final class UpdateAppViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblUpdate: UILabel!
    @IBOutlet weak var btnUpdate: UIButton!
    
    // MARK: - UIViewController
    static func create() -> UIViewController {
        UIStoryboard(name: "UpdateApp", bundle: Bundle.main).instantiateViewController(withIdentifier: "UpdateAppVC")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblUpdate.text = ls.a_new_version_is_available_etc(k.appName)
        btnUpdate.text = ls.update()
        btnUpdate.circularView()
    }
    
    @IBAction func onUpdate(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: "https://itunes.apple.com/us/app/locali/id1510408333?ls=1&mt=8")!)
    }
    
    // MARK: - Deinit
    deinit {
        MyLogD("Dealloc: UpdateAppViewController")
    }
}
