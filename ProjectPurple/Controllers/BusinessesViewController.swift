//
//  BusinessesViewController.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/7/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import UIKit

final class BusinessesViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    // MARK: - Properties
    var businesses = [Business]()
    var foodCategory: FoodCategory?

    // MARK: - Public interface
    static func create(withCategoryFilter category: FoodCategory? = nil) -> UIViewController {
        let viewController = UIStoryboard(name: "Businesses", bundle: nil).instantiateViewController(withIdentifier: "BusinessesVC") as! BusinessesViewController
        viewController.modalPresentationStyle = .fullScreen
        viewController.foodCategory = category
        return viewController
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.attributedText = NSMutableAttributedString()
            .normal(ls.showingSelectionAround())
            .bold("08650")
        btnBack.tintedImage(image: R.image.backArrow()!, color: R.color.localiGreen()!)
        setupCollectionView()
        fetchData()
    }

    private func setupCollectionView() {
        collectionView.register(UINib(nibName: "BusinessCell", bundle: nil), forCellWithReuseIdentifier: "BusinessCell")
        collectionView.dataSource = self
        collectionView.delegate = self

        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 4, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: collectionView.bounds.width, height: k.cellHeight)
        collectionView.collectionViewLayout = layout
    }

    private func fetchData() {
        DataService.instance.getBusinesses { (businesses) in
            onMain {
                self.businesses.removeAll()
                if let foodCategory = self.foodCategory {
                    self.businesses.append(contentsOf: businesses.filter({ $0.categories.contains(foodCategory.id) }))
                } else {
                    self.businesses.append(contentsOf: businesses)
                }
                self.collectionView.reloadData()
            }
        }
    }

    // MARK: - onButton actions
    @IBAction func onBack(_ sender: UIButton) {
        dismiss(animated: true)
    }
}

extension BusinessesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        businesses.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BusinessCell", for: indexPath) as? BusinessCell else {
            fatalError()
        }

        cell.setupCell(businesses[indexPath.row])
        return cell
    }
}

extension BusinessesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        present(ShopViewController.create(with: businesses[indexPath.row]), animated: true)
    }
}

extension BusinessesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace: CGFloat = 10 * 2
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / 1

        return CGSize(width: widthPerItem, height: k.cellHeight)
    }
}
