//
//  AddressSelectorViewController.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/16/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import MapKit
import UIKit

protocol AddressSelectorListener: class {
    func didSelectMapItem(_ mapItem: MKMapItem)
}

final class AddressSelectorViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    // MARK: - Properties
    private var delegate: AddressSelectorListener?
    private var mapItems = [MKMapItem]()
    private var timer: Timer?
    private var lastSearch = ""

    // MARK: - Public interface
    static func create(withDelegate delegate: AddressSelectorListener) -> UIViewController {
        let storyboard =  UIStoryboard(name: "AddressSelector", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AddressSelectorVC") as! AddressSelectorViewController
        viewController.delegate = delegate
        return viewController
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.placeholder = ls.enterYourAddressOrPostalCode()
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
    }

    deinit {
        MyLogD("AddressSelectorVC deallocated")
    }

    // MARK: - Support methods
    private func stopTimer() {
        if timer != nil {
            timer?.invalidate()
        }
    }

    @objc private func updateSearchResults() {
        stopTimer()

        guard let searchText = searchBar.text, searchText.count > 2, searchText != lastSearch else { return }

        MyLogD("AddressSelector: Searching: \(searchText)")

        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = searchText

        let activeSearch = MKLocalSearch(request: searchRequest)
        activeSearch.start { (response, error) in
            if let mapItems = response?.mapItems {
                onMain {
                    self.mapItems.removeAll()
                    self.mapItems.append(contentsOf: mapItems)
                    self.lastSearch = searchText
                    self.tableView.reloadData()
                }
            } else if let error = error {
                MyLogE("AddressSelector: Couldn't get a response. \(error)")
            }
        }
    }
}

extension AddressSelectorViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateSearchResults), userInfo: nil, repeats: true)
    }
}

extension AddressSelectorViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        mapItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "AddressCell")
        }
        let placemark = mapItems[indexPath.row].placemark

        var title = ""
        if let name = placemark.name {
            title += name
        }
        if let city = placemark.locality {
            title += ", \(city)"
        }
        if let postalCode = placemark.postalCode {
            title += ", \(postalCode)"
        }
        if let country = placemark.country {
            title += ", \(country)"
        }
        cell!.textLabel?.text = title
        return cell!
    }
}

extension AddressSelectorViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectMapItem(mapItems[indexPath.row])
        dismiss(animated: true)
    }
}
