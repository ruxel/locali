//
//  ShopViewController.swift
//  ProjectPurple
//
//  Created by Pinto Diaz, Roger on 4/20/20.
//  Copyright © 2020 Hoowie. All rights reserved.
//

import FirebaseFirestore
import UIKit

final class ShopViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, ShopItemCellListener {

    // MARK: - IBOutlets

    @IBOutlet weak var ivHeader: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAvailability: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnBack: UIButton!
    
    // MARK: - Properties
    private var business: Business!
    private var items = [Item]()
    private var filteredItems = [Item]()
    private var visibleItemId: String?
    private var timer: Timer?
    private var lastSearch = ""

    // MARK: - Public interface
    static func create(with business: Business) -> UIViewController {
        let storyboard = UIStoryboard(name: "Shop", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ShopVC") as! ShopViewController
        viewController.business = business
        viewController.modalPresentationStyle = .fullScreen
        return viewController
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        ImageLoadHelper.load(business.imageUrl, in: ivHeader)
        lblName.text = business.name
        lblAvailability.text = business.availability
        btnBack.tintedImage(image: R.image.backArrow()!, color: R.color.localiGreen()!)
        searchBar.placeholder = ls.search_business_name(business.name)
        searchBar.delegate = self

        addTapGestureRecognizerToHideKeyboardTappingOutsideIt()
        setupCollectionView()
        fetchData()
    }

    deinit {
        MyLogD("Dealloc: \(self)")
    }

    // MARK: - UI methods
    private func setupCollectionView() {
        collectionView.register(UINib(nibName: "ShopItemCell", bundle: nil), forCellWithReuseIdentifier: "ShopItemCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.keyboardDismissMode = .onDrag

        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 15, right: 5)
        flowLayout.minimumLineSpacing = 5
        collectionView.collectionViewLayout = flowLayout
    }

    private func clearArrays() {
        items.removeAll()
        filteredItems.removeAll()
    }

    private func fetchData() {
        clearArrays()

        DataService.instance.getItemsFor(businessID: self.business.id) { (items) in
            self.items.append(contentsOf: items)
            self.filteredItems.append(contentsOf: items)

            onMain {
                self.collectionView.reloadData()
            }
        }
    }

    @IBAction func onBack(_ sender: UIButton) {
        dismiss(animated: true)
    }

    private func hideAddProductView() {
        guard visibleItemId != nil else {
            return
        }
        NotificationCenter.default.post(name: k.notification.hideAddProductView, object: nil, userInfo: ["productId": visibleItemId as Any])
    }

    // MARK: - Support methods
    private func stopTimer() {
        if timer != nil {
            timer?.invalidate()
        }
    }

    @objc private func updateSearchResults() {
        stopTimer()

        guard let searchText = searchBar.text, searchText.count > 2, searchText != lastSearch else {
            filteredItems.removeAll()
            filteredItems.append(contentsOf: items)
            collectionView.reloadData()
            return
        }

        MyLogD("ShopVC: Searching: \(searchText)")

        filteredItems.removeAll()
        filteredItems = items.filter({ (item) -> Bool in
            if item.category.contains(searchText) {
                return true

            } else if item.name.contains(searchText) {
                return true

            } else if item.info.contains(searchText) {
                return true

            } else {
                return false
            }
        })

        lastSearch = searchText
        collectionView.reloadData()
    }

    // MARK: - UIScrollViewDelegate
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        hideAddProductView()
    }

    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        filteredItems.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShopItemCell", for: indexPath) as? ShopItemCell else {
            fatalError()
        }
        cell.setupCell(filteredItems[indexPath.row], delegate: self)
        return cell
    }

    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        hideAddProductView()
    }

    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace: CGFloat = (10 * 2)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / 2

        return CGSize(width: widthPerItem, height: widthPerItem * 1.2)
    }

    // MARK: - ShopItemCellListener
    func updateItem(_ item: Item?, quantity: Int) {
        guard let item = item else {
            MyLogE("Couldn't update product: Item is nil")
            return
        }
        DataService.instance.addProductToCart(item, businessId: business.id, quantity: quantity)
    }

    func updateAddViewStatus(visibleItemId: String?) {
        guard let newVisibleItemId = visibleItemId else {
            return
        }

        if self.visibleItemId == nil {
            self.visibleItemId = newVisibleItemId
        } else if newVisibleItemId != self.visibleItemId {
            let userInfo = ["productId": self.visibleItemId as Any]
            NotificationCenter.default.post(name: k.notification.hideAddProductView, object: nil, userInfo: userInfo)
            self.visibleItemId = newVisibleItemId
        }
    }

    // MARK: - UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NotificationCenter.default.post(name: k.notification.hideAllAddProductViews, object: nil, userInfo: nil)
        stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateSearchResults), userInfo: nil, repeats: true)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
